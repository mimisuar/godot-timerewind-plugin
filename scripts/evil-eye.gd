extends Sprite

const ALIVE_TEXTURE = preload("res://graphics/evil-eye.png")
const DEAD_TEXTURE = preload("res://graphics/evil-eye-death.png")

var anim
var tc
const GRAVITY = 15
const MAX_Y_VEL = 5 * 60
var y_vel = 0
var anim_offset = 0

func _ready():
	tc = $TimeControl
	anim = $animation_player
	tc.add_tracker("position")
	tc.add_tracker("y_vel")
	tc.add_tracker("texture")
	tc.add_tracker("frame")
	tc.add_tracker("anim_offset")
	

func _process(dt):

	if tc.is_disabled():
		hide()
	else:
		show()
	
	anim_offset = anim.current_animation_position
	y_vel += GRAVITY
	if y_vel > MAX_Y_VEL:
		y_vel = MAX_Y_VEL
	translate(Vector2(0, y_vel * dt))
	if position.y > get_viewport_rect().size.y:
		position.y = -40
		
	if Input.is_action_just_pressed("kill_eye"):
		texture = DEAD_TEXTURE
		anim.stop(true)
		anim_offset = 0
		anim.play("default")
	
	if texture == DEAD_TEXTURE:
		anim.get_animation("default").loop = false
	else:
		if not anim.is_playing():
			anim.play("default")
		anim.get_animation("default").loop = true
	
	if tc.rewinding:
		tc.rewind()
	else:
		tc.track_time()
	
	anim.seek(anim_offset, true)

func _on_animation_player_animation_finished(anim):
	if texture == DEAD_TEXTURE:
		tc.disable()


func _on_TimeControl_erased_from_time():
	queue_free()
