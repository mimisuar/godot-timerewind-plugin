extends Label

var time_elapsed = 0
var tc

func _ready():
	tc = $TimeControl
	tc.add_tracker("time_elapsed")

func _process(dt):
	
	text = "Time: " + str(time_elapsed)
	
	if tc.rewinding:
		add_color_override("font_color", Color(1.0, 0.5, 0.5))
		tc.rewind()
	else:
		add_color_override("font_color", Color(1.0, 1.0, 1.0))
		time_elapsed += dt
		tc.track_time()