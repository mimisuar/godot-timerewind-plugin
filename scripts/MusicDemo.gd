extends AudioStreamPlayer

const TRACK_NORMAL = preload("res://sounds/haunted.ogg")
const TRACK_REVERSED = preload("res://sounds/haunted-reversed.ogg")
var length

func _ready():
	add_to_group("time_control")
	# this gives us access to the start_rewind() and end_rewind() calls
	# we don't need the full POWAH of the time control module 
	length = TRACK_NORMAL.get_length()

func start_rewind():
	var pos = get_playback_position()
	stop()
	stream = TRACK_REVERSED
	play(length - pos)
	
func end_rewind():
	var pos = get_playback_position()
	stop()
	stream = TRACK_NORMAL
	play(length - pos)