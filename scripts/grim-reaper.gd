extends Sprite

const GRAVITY = 15
const MAX_Y_VEL = 5 * 60
var y_vel = 0


func _process(dt):
	y_vel += GRAVITY
	if y_vel > MAX_Y_VEL:
		y_vel = MAX_Y_VEL
	translate(Vector2(0, y_vel * dt))
	if position.y > get_viewport_rect().size.y:
		position.y = -40
	
	if Input.is_action_just_pressed("rewind"):
		get_tree().call_group("time_control", "start_rewind")
	elif Input.is_action_just_released("rewind"):
		get_tree().call_group("time_control", "end_rewind")