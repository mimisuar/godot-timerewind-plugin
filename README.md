# Time Rewind #

A really simple time rewind plugin for Godot games. You can import this directory, as it is a very simple project showcasing the time rewind.

Using the time control plugin is super easy:

1. Attach a "TimeControl" node to any object that needs to be rewinded.  
2. Add a tracker to each variable you need to rewind.  
3. Include a group call (get_tree().call_group) to "time_control" with the functions "start_rewind" and "end_rewind". (Check grim-reaper.gd to see how it's done).  

There are many limitations (currently). Right now, you can only control variables from the parent node. If you have a kinematic body that has a sprite child, you need 
a proxy variable in the kinematic body. Not sure if that will ever change, maybe in the future when I am more comfortable with Godot.


# How to use in your own project

Add the "addons" folder to your godot project. In the project settings, go to Plugins and activate it. Not too bad :)

# Features and Stuff

You can track different variables (transforms, positions, animation frames, etc.) using 'add_tracker'

The super-duper Disable System (powered by booleans :3) is useful for allowing objects to be "destroyed" without actually removing them from the scene.