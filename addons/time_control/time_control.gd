tool
extends Node

signal erased_from_time # muahaha #
signal on_disable # these two are called during rewind
signal on_enable
signal on_rewind_end
signal on_rewind_start

export (float) var MAX_REWIND_TIME = 5.0
var MAX_FRAMES # 60 fps * 5 seconds = 300 frames of info #
var rewinding = false

var trackers = {}
var tracker_size = 0
var disable_track = []
var disabled = false

func _ready():
	add_to_group("time_control")
	MAX_FRAMES = 60 * MAX_REWIND_TIME
	
func track_time():
	
	for prop_name in trackers:
		
		if trackers[prop_name].size() == MAX_FRAMES:
			trackers[prop_name].pop_front()
			
		var tmp = get_parent().get(prop_name) # debug stuff	
		trackers[prop_name].push_back(tmp)
	tracker_size = clamp(tracker_size + 1, 0, MAX_FRAMES)
	
	track_disable()

func rewind():
	if tracker_size == 0:
		end_rewind()
		return
	
	for prop_name in trackers:
		if trackers[prop_name].size() > 0:
			var tmp = trackers[prop_name].pop_back()
			get_parent().set(prop_name, tmp)
	
	tracker_size = clamp(tracker_size - 1, 0, MAX_FRAMES)
	
	rewind_disable()

func add_tracker(prop_name):
	trackers[prop_name] = []

func remove_tracker(prop_name):
	if trackers.has(prop_name):
		trackers.erase(prop_name)

		
func track_disable():
	if disable_track.size() == MAX_FRAMES:
		disable_track.pop_front()
	disable_track.push_back(disabled)
	if disable_track[0]:
		# rewinding will not allow the object to comeback
		# it's safe to delete it forever 
		emit_signal("erased_from_time")
	
func rewind_disable():
	if disable_track.size() > 0:
		disabled = disable_track.pop_back()

func start_rewind():
	rewinding = true
	emit_signal("on_rewind_start")

func end_rewind():
	rewinding = false
	emit_signal("on_rewind_end")
	
func disable():
	disabled = true
	
func enable():
	disabled = false
	
func is_disabled():
	return disabled