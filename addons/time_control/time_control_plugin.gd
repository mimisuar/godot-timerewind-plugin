tool
extends EditorPlugin



func _enter_tree():
	add_custom_type("TimeControl", "Node", preload("time_control.gd"), preload("icon_timer.png"))
	
func _exit_tree():
	remove_custom_type("TimeControl")